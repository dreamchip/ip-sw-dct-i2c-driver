/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#undef DEBUG

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/i2c.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/io.h>
#include <linux/clk.h>
#include <linux/delay.h>

#define DEFAULT_FREQ 100000
#define TIMEOUT_MS 100
#define FAST_MODE BIT(6)
#define VSCD BIT(5)
#define MAX_SCL_REF BIT(9)
#define MIN_SCL_REF 4

enum DCT_I2C_SPEED {
	DCT_I2C_SPEED_STANDARD = 100000,
	DCT_I2C_SPEED_FAST = 400000,
};

/* NOP */
#define M_NOP 0x00
/* reset busy flag */
#define M_SET_FREE 0x02
/* start reading bytes from slave */
#define M_READ 0x04
/* start writing bytes to slave */
#define M_WRITE 0x05
/* stop transaction */
#define M_STOP 0x06

/* BUSY flag */
#define M_BUSY 0x80

/* no interrupt active */
#define I2C_NO_IRQ 0x00
/* arbitration lost, the transfer has to be repeated */
#define I2C_M_ARBIT_LOST 0x01
/* no acknowledge by slave */
#define I2C_M_NO_ACK 0x02
/* tx data  required in register TX_DATA */
#define I2C_M_TX_DATA 0x03
/* rx data available in register RX_DATA */
#define I2C_M_RX_DATA 0x04
/* command acknowledge interrupt */
#define I2C_M_CMD_ACK 0x05
/* transaction aborted by master before slave performs a NO_ACK */
#define I2C_S_RX_ABORT 0x08
/* if LLD is active: Start Condition detected'*/
#define I2C_S_START_COND 0x08
/* command request */
#define I2C_S_CMD_REQ 0x09

/* Transmit/Receive Data */
#define TX_RX_DATA 0x0000
/* Command/Status */
#define CMD_STATUS 0x0004
/* Master Address Low */
#define M_ADDR_L 0x0008
/* Master Address High */
#define M_ADDR_H 0x000C
/* Slave Address Low */
#define S_ADDR_L 0x0010
/* Slave Address High */
#define S_ADDR_H 0x0014
/*  Configuration Low */
#define CFG_L 0x0018
/* Configuration High */
#define CFG_H 0x001C

struct dct_i2c {
	struct i2c_adapter adap;
	struct device *dev;
	void __iomem *regs;
	struct clk *clk;
	unsigned int frequency;
	int stop;
};

static int dct_i2c_busy(struct dct_i2c *i2c)
{
	return readl(i2c->regs + CMD_STATUS) & M_BUSY ? 1 : 0;
}

static inline int dct_i2c_wait(struct dct_i2c *i2c, u8 expected)
{
	long timeout = TIMEOUT_MS;
	u8 ret;

	do {
		ret = readl(i2c->regs + CMD_STATUS);
		ret &= 0x0f;
		if (ret == expected)
			return 0;
		if (ret != I2C_NO_IRQ) {
#if 0
			dev_err(i2c->dev, "%s: %d, required %d\n", __func__, ret, expected);
#endif
			return ret;
		}
		/* yield cpu for 100KHz (10us) and 400KHz (2.5us) case
		 * currently use this as compromise, better to calc this values
		 * depending on the frequency */
		usleep_range(5, 20);
		timeout--;
	} while (timeout);

#if 0
	dev_err(i2c->dev, "%s: timeout, required %d\n", __func__, expected);
#endif

	return -1;
}

static inline void dct_i2c_write_addr(struct dct_i2c *i2c, u8 addr)
{
	writel(addr << 1, i2c->regs + M_ADDR_L);
	writel(0, i2c->regs + M_ADDR_H);
}

static inline void dct_i2c_stop(struct dct_i2c *i2c)
{
	writel(M_STOP, i2c->regs + CMD_STATUS);
	dct_i2c_wait(i2c, I2C_M_CMD_ACK);
}

static inline void dct_i2c_abort(struct dct_i2c *i2c)
{
	writel(M_SET_FREE, i2c->regs + CMD_STATUS);
	writel(0x17, i2c->regs + CMD_STATUS);
}

static inline void dct_i2c_write16(struct dct_i2c *i2c, u8 cmd, u8 data)
{
	writel(cmd, i2c->regs + CMD_STATUS);
	writel(data, i2c->regs + TX_RX_DATA);
}

static int dct_i2c_wait_and_stop(struct dct_i2c *i2c, u8 cmd)
{
	int ret;

	ret = dct_i2c_wait(i2c, cmd);
	if (ret == I2C_M_ARBIT_LOST)
		return -1;
	if (ret) {
		dct_i2c_stop(i2c);
		return -1;
	}

	return ret;
}

static inline u8 dct_i2c_read16(struct dct_i2c *i2c, u8 cmd)
{
	u8 val;

	val = readl(i2c->regs + TX_RX_DATA);
	writel(cmd, i2c->regs + CMD_STATUS);

	return val;
}

static inline int dct_i2c_read_data(struct dct_i2c *i2c, u8 *data, int length)
{
	int i = 0, ret;
	volatile u8 tmp;

	if (length == 0)
		return 0;

	writel(M_READ, i2c->regs + CMD_STATUS);

	do {
		ret = dct_i2c_wait_and_stop(i2c, I2C_M_RX_DATA);
		if (ret)
			return -1;

		length--;

		if (length)
			data[i++] = dct_i2c_read16(i2c, M_NOP);
		else
			data[i++] = dct_i2c_read16(i2c, M_STOP);

	} while (length);

	dct_i2c_wait(i2c, I2C_M_RX_DATA);
	tmp = readl(i2c->regs + TX_RX_DATA);
	dct_i2c_wait(i2c, I2C_M_CMD_ACK);

	i2c->stop = 1;

	return i;
}

static inline int dct_i2c_write_data(struct dct_i2c *i2c, u8 *data, int length)
{
	int i = 0, ret;

	dct_i2c_write16(i2c, M_WRITE, data[i++]);

	while (length) {
		ret = dct_i2c_wait_and_stop(i2c, I2C_M_TX_DATA);
		if (ret)
			return -1;
		length--;
		if (length == 0)
			break;
		dct_i2c_write16(i2c, M_NOP, data[i++]);
	};

	i2c->stop = 0;

	return i;
}

static int dct_i2c_probe_chip(struct dct_i2c *i2c)
{
	int ret;

	dev_dbg(i2c->dev, "%s:\n", __func__);

	i2c->stop = 0;

	writel(M_WRITE, i2c->regs + CMD_STATUS);
	writel(0, i2c->regs + TX_RX_DATA);

	ret = dct_i2c_wait(i2c, I2C_M_TX_DATA);

	if (ret)
		return -1;

	return 0;
}

static int dct_i2c_read(struct dct_i2c *i2c, struct i2c_msg *msg)
{
	int ret = -1;

	dev_dbg(i2c->dev, "%s: len %d\n", __func__, msg->len);

	ret = dct_i2c_read_data(i2c, msg->buf, msg->len);

	dev_dbg(i2c->dev, "%s: len %d, ret %d\n", __func__, msg->len, ret);

	if (ret != msg->len)
		return -1;

	return 0;
}

static int dct_i2c_write(struct dct_i2c *i2c, struct i2c_msg *msg)
{
	int ret = -1;

	dev_dbg(i2c->dev, "%s:\n", __func__);

	if (msg->len == 0)
		return dct_i2c_probe_chip(i2c);

	ret = dct_i2c_write_data(i2c, msg->buf, msg->len);

	dev_dbg(i2c->dev, "%s: len %d, ret %d\n", __func__, msg->len, ret);

	if (ret != msg->len)
		return -1;

	return 0;
}

static int dct_i2c_xfer_one(struct i2c_adapter *adap, struct i2c_msg *msg)
{
	struct dct_i2c *i2c = adap->algo_data;

	dev_dbg(i2c->dev, "%s:\n", __func__);

	dct_i2c_write_addr(i2c, msg->addr);

	if (msg->flags & I2C_M_RD)
		return dct_i2c_read(i2c, msg);
	else
		return dct_i2c_write(i2c, msg);
}

static int dct_i2c_xfer(struct i2c_adapter *adap, struct i2c_msg *msgs, int num)
{
	struct dct_i2c *i2c = adap->algo_data;
	int ret, i;

	i2c->stop = 0;

	dev_dbg(i2c->dev, "%s: num %d\n", __func__, num);

	if (dct_i2c_busy(i2c)) {
		dct_i2c_stop(i2c);
		dct_i2c_abort(i2c);
	}

	for (i = 0; i < num; i++) {
		ret = dct_i2c_xfer_one(adap, &msgs[i]);
		if (ret)
			return -ETIMEDOUT;
	}

	if (!i2c->stop)
		dct_i2c_stop(i2c);

	return i;
}

static int dct_i2c_init_hw(struct dct_i2c *i2c)
{
	u8 cfg_l = 0;
	u8 cfg_h = 0;
	u32 clk = clk_get_rate(i2c->clk);
	u32 freq = i2c->frequency;
	u32 scl_delay = 0;
	u32 scl_ref;

	dev_dbg(i2c->dev, "%s: sysclk %d Hz\n", __func__, clk);

	if (clk > 100000000) {
		clk /= 8; /* because of VSCD enabled */
		cfg_h = VSCD;
	}

	if (freq == DCT_I2C_SPEED_STANDARD) {
		scl_ref = (DIV_ROUND_UP(clk, freq) - 4 - scl_delay) / 2;
	} else {
		scl_ref = (DIV_ROUND_UP(4 * clk, 3 * freq) - 4 - scl_delay) / 2;
		cfg_h |= FAST_MODE;
	}

	if (scl_ref >= MAX_SCL_REF)
		dev_err(i2c->dev, "scl_ref: too high %x (limit is %lx)\n",
			scl_ref, MAX_SCL_REF);

	if (scl_ref < MIN_SCL_REF)
		dev_err(i2c->dev, "scl_ref: too low %d (limit is %x)\n",
			scl_ref, MIN_SCL_REF);

	/*Note:	Specification iic_ms.pdf (Version 1.6) is discribing this register values by
			using a term clk_ref, which is not part of the equation in the same document.
			From HW perspective clk_ref is created from the scl_ref register value by interpreting
			the scl_ref value as clk_ref[9:1], since we are calculating scl_ref by using the equation
			we just need to program our calculated value to cfg_l/cfg_h as a 9-Bit value which 
			leads to the 10-Bit clk_ref value internaly*/
	cfg_l = scl_ref & 0xFF;
	cfg_h |= (scl_ref >> 8) & 0x1;

	writel(cfg_l, i2c->regs + CFG_L);
	writel(cfg_h, i2c->regs + CFG_H);

	return 0;
}

static u32 dct_i2c_func(struct i2c_adapter *adap)
{
	return I2C_FUNC_I2C | I2C_FUNC_SMBUS_EMUL;
}

static const struct i2c_algorithm dct_i2c_algorithm = {
	.master_xfer = dct_i2c_xfer,
	.functionality = dct_i2c_func,
};

static int dct_i2c_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct device_node *np = pdev->dev.of_node;
	struct dct_i2c *i2c;
	struct resource *res;
	int ret = 0;

	i2c = devm_kzalloc(&pdev->dev, sizeof(struct dct_i2c), GFP_KERNEL);
	if (!i2c) {
		dev_err(dev, "devm_kzalloc for driver_data failed\n");
		return -ENOMEM;
	}

	if (of_property_read_u32(pdev->dev.of_node, "clock-frequency",
				 &i2c->frequency)) {
		dev_err(dev,
			"can't read clock frequency property, use default frequency %d Hz\n",
			DEFAULT_FREQ);
		i2c->frequency = DEFAULT_FREQ;
	}

	if ((i2c->frequency != DCT_I2C_SPEED_STANDARD) &&
	    (i2c->frequency != DCT_I2C_SPEED_FAST)) {
		dev_err(dev,
			"got unsupported clock-frequency from devictree %d Hz set to default %d Hz\n",
			i2c->frequency, DEFAULT_FREQ);
		i2c->frequency = DEFAULT_FREQ;
	}

	i2c->dev = &pdev->dev;
	platform_set_drvdata(pdev, i2c);

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res) {
		dev_err(dev, "platform_get_resource failed\n");
		return -ENODEV;
	}

	dev_info(dev, "devm_ioremap_resource %llx %llx\n", res->start,
		 res->end);

	i2c->regs = devm_ioremap_resource(&pdev->dev, res);
	if (IS_ERR(i2c->regs)) {
		dev_err(&pdev->dev, "missing io memory\n");
		return PTR_ERR(i2c->regs);
	}

	i2c->clk = devm_clk_get_enabled(&pdev->dev, "lpio_clk");
	if (IS_ERR(i2c->clk)) {
		dev_err(&pdev->dev, "failed to get the clk: %ld\n",
			PTR_ERR(i2c->clk));
		return PTR_ERR(i2c->clk);
	}

	strlcpy(i2c->adap.name, "DCT I2C adapter", sizeof(i2c->adap.name));

	i2c->adap.owner = THIS_MODULE;
	i2c->adap.algo = &dct_i2c_algorithm;
	i2c->adap.dev.parent = &pdev->dev;
	i2c->adap.dev.of_node = np;
	i2c->adap.algo_data = i2c;

	ret = dct_i2c_init_hw(i2c);
	if (ret)
		return ret;

	ret = i2c_add_adapter(&i2c->adap);
	if (ret < 0) {
		dev_err(&pdev->dev, "can't add i2c adapter\n");
		return ret;
	}

	return 0;
}

static int dct_i2c_remove(struct platform_device *dev)
{
	struct dct_i2c *i2c = platform_get_drvdata(dev);

	i2c_del_adapter(&i2c->adap);

	return 0;
}

static const struct of_device_id dct_i2c_match[] = {
	{
		.compatible = "dct,dct_i2c",
	},
	{},
};
MODULE_DEVICE_TABLE(of, dct_i2c_match);

static struct platform_driver dct_i2c_driver = {
	.probe		= dct_i2c_probe,
	.remove		= dct_i2c_remove,
	.driver		= {
		.name	= "dct-i2c",
		.of_match_table = dct_i2c_match,
	},
};
module_platform_driver(dct_i2c_driver);

MODULE_DESCRIPTION("DCT I2C bus driver");
MODULE_LICENSE("GPL v2");
